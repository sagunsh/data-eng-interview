import logging
import sys

from py.logocrawler.logocrawler import scrape_logo

if __name__ == '__main__':
    file = sys.stdin
    urls = file.read().split('\n')

    logo_success = 0
    for i, url in enumerate(urls):
        logo = scrape_logo(url)
        print(f'{url},"{logo}"')
        if logo:
            logo_success += 1
            logging.info(f'{url}, logo={logo}')
        else:
            logging.info(f'{url} - Error fetching logo')

    logging.info(f'Total urls = {len(urls)}, Logo found = {logo_success}')
