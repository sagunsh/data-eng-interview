import logging
import re
import sys
from urllib.parse import urljoin, urlparse

import requests
from requests.exceptions import SSLError

# supress ssl warning
requests.packages.urllib3.disable_warnings()

logging.basicConfig(level=logging.INFO)

HEADERS = {
    'user-agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.122 Safari/537.36'}


def generate_urls(url):
    # 4 types of url
    # http://example.com -> url_http
    # https://example.com -> -> url_https
    # http://www.example.com -> url_http_www
    # https://www.example.com -> url_https_www

    if not url.startswith(('http://', 'https://')):
        url_https = 'https://' + url
        url_http = 'http://' + url
    else:
        if url.startswith('http://'):
            url_https = url.replace('http://', 'https://', 1)
            url_http = url
        else:
            url_http = url.replace('https://', 'http://', 1)
            url_https = url

    if '://www.' not in url_http:
        url_http_www = url_http.replace('://', '://www.')
        url_https_www = url_https.replace('://', '://www.')
    else:
        url_http_www = url_http
        url_https_www = url_https
        url_http = url_http.replace('://www.', '://')
        url_https = url_https.replace('://www.', '://')

    return url_http, url_https, url_http_www, url_https_www


def get_response(url, timeout=60):
    try:
        response = requests.get(url, headers=HEADERS, timeout=timeout)
        return response
    except SSLError:
        try:
            response = requests.get(url, headers=HEADERS, timeout=timeout, verify=False)
            return response
        except:
            pass
    except:
        pass


def verify_logo(logo_url):
    logo_response = get_response(logo_url.replace('/\\', '').replace('\\', '').strip())
    if logo_response:
        content_type = logo_response.headers.get('content-type', '').lower()
        if logo_response.ok and 'image' in content_type:
            return True

    return False


def find_logo_from_regex(content, regexes, index_url):
    for regex in regexes:
        logos = re.findall(regex, content, flags=re.DOTALL | re.IGNORECASE)
        for logo in logos:
            # verify url exists and content is image
            logo_url = urljoin(index_url, logo.replace('\\', ''))
            if verify_logo(logo_url):
                return logo_url


def find_logo(response):
    parsed_url = urlparse(response.url)
    index_url = parsed_url.scheme + '://' + parsed_url.netloc

    # remove html comments from response
    clean_response = re.sub('<!--.*?-->', '', response.text, flags=re.DOTALL)

    # search within head first
    head_element = re.findall('<head.*?</head>', clean_response, flags=re.DOTALL | re.IGNORECASE)
    head = head_element[0] if head_element else '<head></head>'

    # regex for possible logo
    regexes = [
        '"logo"\s*\:\s*"(.*?)"',  # script tags "logo": "logo url"
        '\="?([\w\./\-\:]*?logo[\w\./\-\:]*?(?:\.png|\.svg|\.jpe?g).*?)"?',  # logoxxx.png, logoxxx.svg
        '"url"\s*\:\s*"([\w\./\-\:]*?[\w\./\-\:]*?(?:\.png|\.svg|\.jpe?g).*?)"',  # "url": "logo url"
    ]

    logo_url = find_logo_from_regex(head, regexes, index_url)
    if logo_url:
        return logo_url

    # extract logo from img tags containing any attr with logo substring
    imgs = re.findall('<img.*?>', clean_response, flags=re.DOTALL | re.IGNORECASE)
    for img in imgs:
        # check if logo is present in any attr
        if re.findall('[A-Za-z]*?=".*?logo.*?"', img):
            # extract src attr
            src = re.findall('src="(.*?)"', img)
            if src:
                # verify url exists and content is image
                logo_url = urljoin(index_url, src[0])
                if verify_logo(logo_url):
                    return logo_url

    # regex for possible logo
    regexes = [
        '"logo"\s*\:\s*"(.*?)"',  # script tags "logo": "logo url"
        '\="?([\w\./\-\:]*?logo[\w\./\-\:]*?(?:\.png|\.svg|\.ico|\.jpe?g).*?)"?',  # logoxxx.png, logoxxx.svg
        '\="?([\w\./\-\:]*?icon[\w\./\-\:]*?(?:\.png|\.svg|\.ico|\.jpe?g).*?)"?',  # favicons
        '<link.*?rel="[^"]*?icon.*?href="(.*?)"',  # from link tags <link rel="shortcut icon" href="logo_url" />
        '"url"\s*\:\s*"([\w\./\-\:]*?[\w\./\-\:]*?(?:\.png|\.svg|\.ico|\.jpe?g).*?)"',  # "url": "logo url"
    ]

    logo_url = find_logo_from_regex(clean_response, regexes, index_url)
    if logo_url:
        return logo_url

    logo_url = urljoin(index_url, 'favicon.ico')
    if verify_logo(logo_url):
        return logo_url


def scrape_logo(url):
    possible_urls = generate_urls(url)
    for each_url in possible_urls:
        response = get_response(each_url)
        if response:
            if response.ok:
                logo = find_logo(response)
                if logo:
                    return logo

    return ''


if __name__ == '__main__':
    file = sys.stdin
    # sys.stdout = open('logo.csv', 'w')
    urls = file.read().split('\n')
    logo_success = 0
    for i, url in enumerate(urls):
        logo = scrape_logo(url)
        print(f'{url},"{logo}"')
        if logo:
            logo_success += 1
            logging.info(f'{url}, logo={logo}')
        else:
            logging.info(f'{url} - Error fetching logo')

    logging.info(f'Total urls = {len(urls)}, Logo found = {logo_success}')
